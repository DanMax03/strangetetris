#ifndef _STRANGE_TETRIS_PLAYER_ACTIONS_HPP
#define _STRANGE_TETRIS_PLAYER_ACTIONS_HPP

namespace strange_tetris {
    namespace _details {
        enum class PlayerActions {
            kNone,
            kEndGame,
            kPause,
            kRight,
            kLeft,
            kDown,
            kRotate
        };
    }
}

#endif // _STRANGE_TETRIS_PLAYER_ACTIONS_HPP