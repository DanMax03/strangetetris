namespace strange_tetris {
    namespace _details {
        template<size_t N, size_t M>
        Game<N, M>::Game(std::shared_ptr<InputAdapter> input, std::shared_ptr<OutputAdapter> output,
                std::unique_ptr<GameInformation> info):
                field(std::vector<std::vector<Brick>> (N, std::vector<Brick>(M))),
                figure(nullptr),
                input(input), output(output),
                info(info.release()) {
            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < M; ++j) {
                    this->field[i][j].position = {j, i};
                }
            }
        }

        template<size_t N, size_t M>
        void Game<N, M>::run() {
            start();

            auto current_iteration_start = std::chrono::steady_clock::now();

            while (!isFinished()) {
                if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() -
                        current_iteration_start) >= info->getTimeForIteration()) {
                    current_iteration_start = std::chrono::steady_clock::now();
                    iterationUpdate();
                }
                update(input->handlePlayerEvent());
                output->draw(info, field, figure, N, M);
            }

            end();
        }
    }
}