namespace strange_tetris {
    namespace classic {
        template<size_t N, size_t M>
        ClassicGame<N, M>::ClassicGame(std::shared_ptr<_details::InputAdapter> input,
                std::shared_ptr<_details::OutputAdapter> output):
                _details::Game<N, M>(input, output, std::make_unique<ClassicGameInformation>()) {}

        template<size_t N, size_t M>
        bool ClassicGame<N, M>::isFinished() {
            return !canSpawn || static_cast<ClassicGameInformation*>(this->info.get())->score > 5000;
        }

        template<size_t N, size_t M>
        void ClassicGame<N, M>::start() {
            this->figure.reset(this->info->figure_gen.generateFigure());
            this->figure->position = {M / 2, this->figure->getLimit(_details::Directions::kTop)};
        }

        template<size_t N, size_t M>
        void ClassicGame<N, M>::iterationUpdate() {
            using namespace _details;

            if (this->figure == nullptr) {
                std::unique_ptr<BaseFigure> new_figure(this->info->figure_gen.generateFigure());
                new_figure->position = {M / 2, new_figure->getLimit(Directions::kTop)};

                if (hasCollision(this->field, new_figure)) {
                    canSpawn = false;
                } else {
                    this->figure.reset(new_figure.release());
                }

                return;
            }

            bool trySlice = this->figure->position.y + this->figure->getLimit(Directions::kBottom) == N - 1;

            if (!trySlice) {
                ++this->figure->position.y;
                if (hasCollision(this->field, this->figure)) {
                    trySlice = true;
                    --this->figure->position.y;
                }
            }

            if (!trySlice) return;

            std::vector<Brick> fig_bricks = this->figure->getAbsoluteBricks();
            this->figure.reset(nullptr);

            for (Brick& brick : fig_bricks) {
                this->field[brick.position.y][brick.position.x] = brick;
            }

            bool isFull;
            std::queue<size_t> q;
            int i;

            for (i = N - 1; i >= 0; --i) {
                isFull = true;

                for (int j = 0; isFull && j < M; ++j) {
                    if (this->field[i][j].color == Colors::kNoBlock) {
                        isFull = false;
                    }
                }

                if (!isFull) {
                    q.push(i);
                }
            }

            unsigned long int& score = static_cast<ClassicGameInformation*>(this->info.get())->score;
            unsigned long int delta = 150 * (N - q.size());

            for (size_t border = 4000; border > 0; border -= 1000) {
                if (score < border && score + delta >= border) {
                    this->info->changeTimeForIteration(-60ms);
                }
            }

            score += delta;

            size_t k = N;

            while (!q.empty()) {
                --k;
                if (q.front() != k) {
                    std::swap(this->field[q.front()], this->field[k]);
                }
                q.pop();
            }

            for (i = k - 1; i >= 0; --i) {
                for (int j = 0; j < M; ++j) {
                    this->field[i][j].color = Colors::kNoBlock;
                    this->field[i][j].position = {j, i};
                }
            }

            for (i = k; i < N; ++i) {
                for (int j = 0; j < M; ++j) {
                    this->field[i][j].position = {j, i};
                }
            }
        }

        template<size_t N, size_t M>
        void ClassicGame<N, M>::update(_details::PlayerActions action) {
            if (this->figure == nullptr) return;

            using namespace _details;

            switch (action) {
                case PlayerActions::kDown: {
                    bool wasInside = false;
                    while (this->figure->position.y + this->figure->getLimit(Directions::kBottom) < N - 1 &&
                            !hasCollision(this->field, this->figure)) {
                        wasInside = true;
                        ++this->figure->position.y;
                    }
                    if (wasInside) {
                        --this->figure->position.y;
                    }
                    break;
                }

                case PlayerActions::kLeft: {
                    if (this->figure->position.x - this->figure->getLimit(Directions::kLeft) > 0) {
                        --this->figure->position.x;
                        if (hasCollision(this->field, this->figure)) {
                            ++this->figure->position.x;
                        }
                    }
                    break;
                }

                case PlayerActions::kRight: {
                    if (this->figure->position.x + this->figure->getLimit(Directions::kRight) < M - 1) {
                        ++this->figure->position.x;
                        if (hasCollision(this->field, this->figure)) {
                            --this->figure->position.x;
                        }
                    }
                    break;
                }

                case PlayerActions::kRotate: {
                    std::unique_ptr<BaseFigure> copy =
                            std::make_unique<BaseFigure>(*(this->figure.get()));
                    copy->rotate();

                    if (copy->position.y + copy->getLimit(Directions::kBottom) >= N) {
                        copy->position.y -= copy->position.y + copy->getLimit(Directions::kBottom) - N + 1;
                    }
                    if (copy->position.y - copy->getLimit(Directions::kTop) < 0) {
                        copy->position.y += copy->getLimit(Directions::kTop) - copy->position.y;
                    }
                    if (copy->position.x + copy->getLimit(Directions::kRight) >= M) {
                        copy->position.x -= copy->position.x + copy->getLimit(Directions::kRight) - M + 1;
                    }
                    if (copy->position.x - copy->getLimit(Directions::kLeft) < 0) {
                        copy->position.x += copy->getLimit(Directions::kLeft) - copy->position.x;
                    }

                    if (!hasCollision(this->field, copy)) {
                        this->figure->rotate();
                        this->figure->position = copy->position;
                    }
                    break;
                }

                case PlayerActions::kEndGame: {
                    canSpawn = false;
                    break;
                }

                default: break;
            }
        }

        template<size_t N, size_t M>
        void ClassicGame<N, M>::end() {
            ClassicOutputAdapter* out = static_cast<ClassicOutputAdapter*>(this->output.get());

            std::string s = "The game has ended successfully!\n";
            s += "The final score is: " + std::to_string(static_cast<ClassicGameInformation*>(this->info.get())->score);
            s += '\n';

            out->printMessage(s);
        }
    }
}
