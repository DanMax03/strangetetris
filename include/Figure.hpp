#ifndef _STRANGE_TETRIS_FIGURE_HPP
#define _STRANGE_TETRIS_FIGURE_HPP

#include <memory>
#include <array>
#include <vector>

namespace strange_tetris {
    namespace _details {
        enum class Colors {
            kNoBlock,  // must be always the first
            kWhite,
            kRed,
            kRose,
            kYellow,
            kOrange,
            kBlue,
            kDarkBlue,
            kGreen,
            kCyan,
            Count  // not color, must be always the last
        };

        /* Axes:
         *   -
         * - 0 + x
         *   -
         *   y
         * The axes center on a field is left top corner
         */
        struct Point {
            int x = 0;
            int y = 0;

            Point& operator+=(const Point& other) {
                x += other.x;
                y += other.y;
                return *this;
            }

            friend bool operator==(const Point& a, const Point& b) {
                return a.x == b.x && a.y == b.y;
            }

            friend bool operator!=(const Point& a, const Point& b) {
                return a.x != b.x || a.y != b.y;
            }
        };

        struct Brick {
            Point position;
            Colors color = Colors::kNoBlock;

            Brick() = default;
            Brick(Colors color): color(color) {}
        };

        enum class Directions {
            kTop,
            kRight,
            kBottom,
            kLeft
        };

        class BaseFigure {
        public:
            Point position;

            BaseFigure();
            BaseFigure(size_t count, Colors color);

            virtual void rotate();

            std::vector<Brick> getAbsoluteBricks() const;
            int getLimit(Directions dir) const;

            virtual ~BaseFigure() = default;
        protected:
            std::vector<Brick> bricks;
            size_t center;
            std::array<int, 4> limits;
            /* I don't want to write static_cast<size_t>(enum_var) everywhere, so:
             * 0 - Top
             * 1 - Right
             * 2 - Bottom
             * 3 - Left
             */
        };


        bool hasCollision(const std::vector<std::vector<Brick>>& field, const std::unique_ptr<BaseFigure>& fig_ptr);


        /*
         *   2 3
         * 0 1
         */
        class SFigure : public BaseFigure {
        public:
            SFigure(Colors color);
            SFigure(const SFigure&) = default;
        };

        /*
         * 0 1
         *   2 3
         */
        class ZFigure : public BaseFigure {
        public:
            ZFigure(Colors color);
            ZFigure(const ZFigure&) = default;
        };

        /*
         * 0 1 2
         *   3
         */
        class TFigure : public BaseFigure {
        public:
            TFigure(Colors color);
            TFigure(const TFigure&) = default;
        };

        /*
         * 0
         * 1
         * 2 3
         */
        class LFigure : public BaseFigure {
        public:
            LFigure(Colors color);
            LFigure(const LFigure&) = default;
        };

        /*
         *   0
         *   1
         * 3 2
         */
        class JFigure : public BaseFigure {
        public:
            JFigure(Colors color);
            JFigure(const JFigure&) = default;
        };

        /*
         * Enumeration from left to right
         */
        class LineFigure : public BaseFigure {
        public:
            LineFigure(Colors color, size_t length = 4);
            LineFigure(const LineFigure&) = default;
        };

        /*
         * Enumeration from left to right and from top to bottom
         */
        class SquareFigure : public BaseFigure {
        public:
            SquareFigure(Colors color, size_t side = 2);
            SquareFigure(const SquareFigure&) = default;
        };
    }
}

#endif // _STRANGE_TETRIS_FIGURE_HPP