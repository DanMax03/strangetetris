#ifndef _STRANGE_TETRIS_OUTPUT_ADAPTER_HPP
#define _STRANGE_TETRIS_OUTPUT_ADAPTER_HPP

#include <memory>
#include <vector>
#include "Figure.hpp"
#include "GameInformation.hpp"

namespace strange_tetris {
    namespace _details {
        class OutputAdapter {
        public:
            virtual void draw(const std::unique_ptr<GameInformation>& info,
                              const std::vector<std::vector<Brick>>& field,
                              const std::unique_ptr<BaseFigure>& figure,
                              const size_t N, const size_t M) = 0;

            virtual ~OutputAdapter() = default;
        };
    }

    class EmptyOutput final : public _details::OutputAdapter {
    public:
        void draw(const std::unique_ptr<_details::GameInformation>& info,
                  const std::vector<std::vector<_details::Brick>>& field,
                  const std::unique_ptr<_details::BaseFigure>& figure,
                  const size_t N, const size_t M) final {}
    };
}

#endif // _STRANGE_TETRIS_OUTPUT_ADAPTER_HPP