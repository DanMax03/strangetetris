//
// Created by daniil on 04.04.2022.
//

#ifndef _STRANGE_TETRIS_CLASSIC_GAME_HPP
#define _STRANGE_TETRIS_CLASSIC_GAME_HPP

#include "Game.hpp"
#include "ClassicOutputAdapter.hpp"

#include <queue>

namespace strange_tetris {
    namespace classic {
        class ClassicGameInformation : public _details::GameInformation {
        public:
            unsigned long int score;
        };

        template<size_t N, size_t M>
        class ClassicGame : public _details::Game<N, M> {
        public:
            ClassicGame(std::shared_ptr<_details::InputAdapter> input, std::shared_ptr<_details::OutputAdapter> output);
        protected:
            bool canSpawn = true;

            bool isFinished() override;
            void start() override;
            void iterationUpdate() override;
            void update(_details::PlayerActions action) override;
            void end() override;
        };
    }

    namespace user {
        using classic::ClassicGame;
    }
}

#include "tsrc/ClassicGame.tpp"

#endif // _STRANGE_TETRIS_CLASSIC_GAME_HPP
