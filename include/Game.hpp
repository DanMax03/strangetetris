#ifndef _STRANGE_TETRIS_GAME_HPP
#define _STRANGE_TETRIS_GAME_HPP

#include "PlayerActions.hpp"
#include "Figure.hpp"
#include "GameInformation.hpp"
#include "InputAdapter.hpp"
#include "OutputAdapter.hpp"

#include <memory>
#include <vector>
#include <chrono>

namespace strange_tetris {
    namespace _details {
        template<size_t N, size_t M> // N rows, M columns
        class Game {
        public:
            Game(std::shared_ptr<InputAdapter> input, std::shared_ptr<OutputAdapter> output,
                    std::unique_ptr<GameInformation> info = std::make_unique<GameInformation>());

            void run();

            virtual ~Game() = default;
        protected:
            std::vector<std::vector<Brick>> field;
            std::unique_ptr<BaseFigure> figure;
            std::shared_ptr<InputAdapter> input;
            std::shared_ptr<OutputAdapter> output;
            std::unique_ptr<GameInformation> info;

            virtual bool isFinished() = 0;
            virtual void start() = 0;
            virtual void iterationUpdate() = 0;
            virtual void update(PlayerActions action) = 0;
            virtual void end() = 0;
        };
    }
}

#include "tsrc/Game.tpp"

#endif // _STRANGE_TETRIS_GAME_HPP