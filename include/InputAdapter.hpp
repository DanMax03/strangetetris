#ifndef _STRANGE_TETRIS_INPUT_ADAPTER_HPP
#define _STRANGE_TETRIS_INPUT_ADAPTER_HPP

#include "PlayerActions.hpp"

namespace strange_tetris {
    namespace _details {
        class InputAdapter {
        public:
            virtual PlayerActions handlePlayerEvent() = 0;

            virtual ~InputAdapter() = default;
        };
    }

    class EmptyInput final : public _details::InputAdapter {
    public:
        _details::PlayerActions handlePlayerEvent() final { return _details::PlayerActions::kNone; }
    };
}

#endif // _STRANGE_TETRIS_INPUT_ADAPTER_HPP