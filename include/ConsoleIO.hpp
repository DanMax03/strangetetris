#ifndef _STRANGE_TETRIS_CONSOLE_IO_HPP
#define _STRANGE_TETRIS_CONSOLE_IO_HPP

#include "ClassicGame.hpp"
#include "InputAdapter.hpp"
#include <ncurses.h>
#include <iostream>

namespace console_io {
    class StandartConsoleWindow {
    public:
        StandartConsoleWindow();

        ~StandartConsoleWindow();
    };

    class ConsoleInput : public strange_tetris::_details::InputAdapter {
    public:
        ConsoleInput() = default;

        strange_tetris::_details::PlayerActions handlePlayerEvent() override;

        ~ConsoleInput() = default;
    };

    class ClassicConsoleOutput : public strange_tetris::classic::ClassicOutputAdapter {
    public:
        std::string buffer;

        ClassicConsoleOutput(): buffer("") {}

        void draw(const std::unique_ptr<strange_tetris::_details::GameInformation>& info,
                  const std::vector<std::vector<strange_tetris::_details::Brick>>& field,
                  const std::unique_ptr<strange_tetris::_details::BaseFigure>& figure,
                  const size_t N, const size_t M) override;

        void printMessage(const std::string& message) override;
    };

    void addborderch(char ch);
    void addborderstr(std::string& s);
    void drawBrick(const strange_tetris::_details::Brick& br);
}

#endif // _STRANGE_TETRIS_CONSOLE_IO_HPP