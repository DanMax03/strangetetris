//
// Created by daniil on 09.04.2022.
//

#include "OutputAdapter.hpp"

#ifndef _STRANGE_TETRIS_CLASSIC_GAME_OUTPUT_ADAPTER_HPP
#define _STRANGE_TETRIS_CLASSIC_GAME_OUTPUT_ADAPTER_HPP

namespace strange_tetris {
    namespace classic {
        class ClassicOutputAdapter : public _details::OutputAdapter {
        public:
            virtual void printMessage(const std::string& message) = 0;
        };
    }
}

#endif // _STRANGE_TETRIS_CLASSIC_GAME_OUTPUT_ADAPTER_HPP
