#ifndef _STRANGE_TETRIS_FIGURE_GENERATOR_HPP
#define _STRANGE_TETRIS_FIGURE_GENERATOR_HPP

#include "Figure.hpp"

#include <deque>
#include <chrono>
#include <random>

namespace strange_tetris {
    namespace _details {
        class FigureGenerator {
        public:
            FigureGenerator(): gen(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())) {}

            BaseFigure* generateFigure();

            virtual ~FigureGenerator() = default;
        protected:
            std::mt19937 gen;
        };
    }
}

#endif // _STRANGE_TETRIS_FIGURE_GENERATOR_HPP