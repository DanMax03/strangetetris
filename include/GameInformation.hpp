#ifndef _STRANGE_TETRIS_GAME_INFORMATION_HPP
#define _STRANGE_TETRIS_GAME_INFORMATION_HPP

#include "FigureGenerator.hpp"
#include <chrono>

namespace strange_tetris {
    namespace _details {
        using namespace std::chrono_literals;

        class GameInformation {
        public:
            using milliseconds = std::chrono::milliseconds;

            FigureGenerator figure_gen;

            GameInformation();

            milliseconds getTimeForIteration() const;
            void setTimeForIteration(milliseconds new_duration);
            void changeTimeForIteration(milliseconds delta);

            virtual ~GameInformation() = default;
        protected:
            const milliseconds bottom_bound = 100ms;
            const milliseconds top_bound = 1000ms;
            const milliseconds std_time_for_iteration = 500ms;

            milliseconds time_for_iteration;
        };
    }
}

#endif // _STRANGE_TETRIS_GAME_INFORMATION_HPP