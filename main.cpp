#include "ClassicGame.hpp"
#include "ConsoleIO.hpp"

#include <iostream>

int main() {
    std::string final_str;
    {
        using namespace console_io;
        StandartConsoleWindow win;

        std::shared_ptr<strange_tetris::_details::InputAdapter> input = std::make_unique<ConsoleInput>();
        std::shared_ptr<strange_tetris::_details::OutputAdapter> output = std::make_unique<ClassicConsoleOutput>();
        strange_tetris::user::ClassicGame<15, 10> game(input, output);

        game.run();

        final_str = static_cast<ClassicConsoleOutput*>(output.get())->buffer;
    }

    std::cout << final_str;

    return 0;
}
