#include "Figure.hpp"

namespace strange_tetris {
    namespace _details {
        BaseFigure::BaseFigure(): center(0) {
            limits.fill(0);
        }

        BaseFigure::BaseFigure(size_t count, Colors color): center(0) {
            bricks.assign(count, Brick(color));
            limits.fill(0);
        }

        void BaseFigure::rotate() {
            for (Brick& brick : bricks) {
                std::swap(brick.position.x, brick.position.y);
                brick.position.x = -brick.position.x;
            }

            int buf = limits[3];
            limits[3] = limits[2];
            limits[2] = limits[1];
            limits[1] = limits[0];
            limits[0] = buf;
        }

        std::vector<Brick> BaseFigure::getAbsoluteBricks() const {
            std::vector<Brick> copy = bricks;

            for (Brick& brick : copy) {
                brick.position += position;
            }

            return copy;
        }

        int BaseFigure::getLimit(Directions dir) const {
            return limits[static_cast<size_t>(dir)];
        }

        bool hasCollision(const std::vector<std::vector<Brick>>& field, const std::unique_ptr<BaseFigure>& fig_ptr) {
            std::vector<Brick> fig_bricks = fig_ptr->getAbsoluteBricks();

            for (Brick& brick : fig_bricks) {
                if (field[brick.position.y][brick.position.x].color != Colors::kNoBlock) {
                    return true;
                }
            }

            return false;
        }

        SFigure::SFigure(Colors color): BaseFigure(4, color) {
            center = 1;
            limits[0] = 1;
            limits[1] = 1;
            limits[2] = 0;
            limits[3] = 1;
            bricks[0].position = {-1, 0};
            bricks[2].position = {0, -1};
            bricks[3].position = {1, -1};
        }

        ZFigure::ZFigure(Colors color): BaseFigure(4, color) {
            center = 2;
            limits[0] = 1;
            limits[1] = 1;
            limits[2] = 0;
            limits[3] = 1;
            bricks[0].position = {-1, -1};
            bricks[1].position = {0, -1};
            bricks[3].position = {1, 0};
        }

        TFigure::TFigure(Colors color): BaseFigure(4, color) {
            center = 1;
            limits[0] = 0;
            limits[1] = 1;
            limits[2] = 1;
            limits[3] = 1;
            bricks[0].position = {-1, 0};
            bricks[2].position = {1, 0};
            bricks[3].position = {0, 1};
        }

        LFigure::LFigure(Colors color): BaseFigure(4, color) {
            center = 1;
            limits[0] = 1;
            limits[1] = 1;
            limits[2] = 1;
            limits[3] = 0;
            bricks[0].position = {0, -1};
            bricks[2].position = {0, 1};
            bricks[3].position = {1, 1};
        }

        JFigure::JFigure(Colors color): BaseFigure(4, color) {
            center = 1;
            limits[0] = 1;
            limits[1] = 0;
            limits[2] = 1;
            limits[3] = 1;
            bricks[0].position = {0, -1};
            bricks[2].position = {0, 1};
            bricks[3].position = {-1, 1};
        }

        LineFigure::LineFigure(Colors color, size_t length): BaseFigure(length, color) {
            center = length / 2;
            limits[0] = 0;
            limits[1] = length - 1 - length / 2;
            limits[2] = 0;
            limits[3] = length / 2;

            int x = -length / 2;

            for (Brick& brick : bricks) {
                brick.position.x = x;
                ++x;
            }
        }

        SquareFigure::SquareFigure(Colors color, size_t side): BaseFigure(side * side, color) {
            center = side * (side / 2) + side / 2;
            limits[0] = side / 2;
            limits[1] = side - 1 - side / 2;
            limits[2] = side - 1 - side / 2;
            limits[3] = side / 2;

            int x;
            int y = -side / 2;

            for (size_t i = 0; i < side; ++i, ++y) {
                x = -side / 2;
                for (size_t j = 0; j < side; ++j, ++x) {
                    bricks[i * side + j].position = {x, y};
                }
            }
        }
    }
}