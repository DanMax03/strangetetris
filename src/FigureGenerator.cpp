#include "FigureGenerator.hpp"

namespace strange_tetris {
    namespace _details {
        BaseFigure* FigureGenerator::generateFigure() {
            BaseFigure* p;
            static std::uniform_int_distribution<> fig_distrib(0, 6);
            static std::uniform_int_distribution<> color_distrib(1, static_cast<size_t>(Colors::Count) - 1);

            Colors color = static_cast<Colors>(color_distrib(gen));

            switch (fig_distrib(gen)) {
                case 0:
                    p = new LineFigure(color);
                    break;

                case 1:
                    p = new SFigure(color);
                    break;

                case 2:
                    p = new ZFigure(color);
                    break;

                case 3:
                    p = new TFigure(color);
                    break;

                case 4:
                    p = new LFigure(color);
                    break;

                case 5:
                    p = new JFigure(color);
                    break;

                case 6:
                    p = new SquareFigure(color);
                    break;
            }

            return p;
        }
    }
}