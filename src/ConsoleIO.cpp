#include "ConsoleIO.hpp"

namespace console_io {
    StandartConsoleWindow::StandartConsoleWindow() {
        initscr();
        start_color();
        cbreak();
        noecho();
        nodelay(stdscr, true);
        keypad(stdscr, true);

        // Colors
        init_pair(0, COLOR_BLACK, COLOR_WHITE);
        init_pair(1, COLOR_WHITE, COLOR_WHITE);
        init_pair(2, COLOR_RED, COLOR_RED);
        init_pair(3, COLOR_YELLOW, COLOR_YELLOW);
        init_pair(4, COLOR_BLUE, COLOR_BLUE);
        init_pair(5, COLOR_GREEN, COLOR_GREEN);
        init_pair(6, COLOR_CYAN, COLOR_CYAN);
    }

    StandartConsoleWindow::~StandartConsoleWindow() {
        endwin();
    }

    strange_tetris::_details::PlayerActions ConsoleInput::handlePlayerEvent() {
        using strange_tetris::_details::PlayerActions;

        int ch = getch();

        switch (ch) {
            case 'a':
                return PlayerActions::kLeft;
            case 'r':
                return PlayerActions::kRotate;
            case 'd':
                return PlayerActions::kRight;
            case 's':
                return PlayerActions::kDown;
            case 'q':
                return PlayerActions::kEndGame;
            default:
                return PlayerActions::kNone;
        }
    }

    void ClassicConsoleOutput::printMessage(const std::string& message) {
        buffer += message;
    }

    void addborderch(char ch) {
        attron(A_BOLD);
        attron(COLOR_PAIR(0));
        addch(ch);
        attroff(A_BOLD);
        attroff(COLOR_PAIR(0));
    }

    void addborderstr(std::string& s) {
        attron(A_BOLD);
        attron(COLOR_PAIR(0));
        addstr(s.c_str());
        attroff(A_BOLD);
        attroff(COLOR_PAIR(0));
    }

    void drawBrick(const strange_tetris::_details::Brick& br) {
        using namespace strange_tetris::_details;
        int color_pair_index;
        char br_char = '@';

        if (br.color == Colors::kNoBlock) {
            mvaddch(br.position.y, br.position.x + 1, '.');
            return;
        }

        switch (br.color) {
            case Colors::kRed:
            case Colors::kRose: {
                color_pair_index = 2;
                break;
            }

            case Colors::kYellow:
            case Colors::kOrange: {
                color_pair_index = 3;
                break;
            }

            case Colors::kBlue:
            case Colors::kDarkBlue: {
                color_pair_index = 4;
                break;
            }

            case Colors::kGreen: {
                color_pair_index = 5;
                break;
            }

            case Colors::kCyan: {
                color_pair_index = 6;
                break;
            }

            default: {
                color_pair_index = 1;
                break;
            }
        }

        attron(COLOR_PAIR(color_pair_index));
        mvaddch(br.position.y, br.position.x + 1, br_char);
        attroff(COLOR_PAIR(color_pair_index));
    }

    void ClassicConsoleOutput::draw(const std::unique_ptr<strange_tetris::_details::GameInformation>& info,
                                    const std::vector<std::vector<strange_tetris::_details::Brick>>& field,
                                    const std::unique_ptr<strange_tetris::_details::BaseFigure>& figure,
                                    const size_t N, const size_t M) {
        using namespace strange_tetris::_details;

        // A field in general
        std::string s = "";
        s.assign(M, '.');

        for (size_t i = 0; i < N; ++i) {
            addborderch('#');
            addstr(s.c_str());
            addborderch('#');
            addch('\n');
        }

        s = "";
        s.append(M + 2, '#');
        s += "\n";
        addborderstr(s);

        // The field's bricks
        for (size_t i = 0; i < N; ++i) {
            for (size_t j = 0; j < M; ++j) {
                drawBrick(field[i][j]);
            }
        }

        // A player's figure
        if (figure != nullptr) {
            std::vector<Brick> fig_bricks = figure->getAbsoluteBricks();
            for (Brick& brick: fig_bricks) {
                drawBrick(brick);
            }
        }

        s = "Total score: " +
                std::to_string(static_cast<strange_tetris::classic::ClassicGameInformation*>(info.get())->score) + '\n';

        mvaddstr(N + 1, 0, s.c_str());

        move(0, 0);

        refresh();
    }
}