#include "GameInformation.hpp"
#include "gtest/gtest.h"

using namespace strange_tetris::_details;

class TestGameInformation : public GameInformation {
public:
    using GameInformation::GameInformation;

    bool isEqualToBottom() const {
        return time_for_iteration == bottom_bound;
    }
    bool isEqualToTop() const {
        return time_for_iteration == top_bound;
    }
    bool isEqualToStd() const {
        return time_for_iteration == std_time_for_iteration;
    }
};

TEST(GameInformationTestSuite, BordersTest) {
    TestGameInformation gi;

    ASSERT_TRUE(gi.isEqualToStd());

    gi.changeTimeForIteration(-1000ms);
    ASSERT_TRUE(gi.isEqualToBottom());

    gi.changeTimeForIteration(1000ms);
    ASSERT_TRUE(gi.isEqualToTop());
}