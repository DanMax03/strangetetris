#include "FigureGenerator.hpp"
#include "gtest/gtest.h"

#include <unordered_set>

using namespace strange_tetris::_details;

template<typename... Types>
struct not_empty : std::true_type {};

template<>
struct not_empty<> : std::false_type {};

template<typename Head, typename... Tail>
size_t getRealTypeIndex(BaseFigure* fig) {
    if (dynamic_cast<Head*>(fig) != nullptr) {
        return 0;
    } else if constexpr (not_empty<Tail...>::value) {
        return 1 + getRealTypeIndex<Tail...>(fig);
    } else {
        return -1;
    }
}

TEST(FigureGeneratorTestSuite, GeneralTest) {
    FigureGenerator gen;
    std::unique_ptr<BaseFigure> fig;
    std::unordered_set<size_t> a;

    while (a.size() != 7) {
        fig.reset(gen.generateFigure());

        size_t v = getRealTypeIndex<LineFigure, SFigure, ZFigure, TFigure, LFigure, JFigure, SquareFigure>(fig.get());

        if (v == 8) {
            ASSERT_TRUE(false);
        }

        a.insert(v);
    }
}