#include "Figure.hpp"
#include "gtest/gtest.h"

#include <array>

using namespace strange_tetris::_details;

template<typename FigureType>
class TestFigure : public FigureType {
public:
    using FigureType::FigureType;

    std::vector<Brick>& accessBricks() {
        return this->bricks;
    }
    size_t getCenter() {
        return this->center;
    }
    std::array<int, 4>& getLimits() {
        return this->limits;
    }
};

template<typename FigureType>
bool checkLimits(TestFigure<FigureType>& fig) {
    std::array<int, 4> res = {0, 0, 0, 0};
    std::vector<Brick> bricks = fig.accessBricks();

    for (Brick& brick : bricks) {
        res[0] = std::min(res[0], brick.position.y); // Top
        res[1] = std::max(res[1], brick.position.x); // Left
        res[2] = std::max(res[2], brick.position.y); // Bottom
        res[3] = std::min(res[3], brick.position.x); // Right
    }

    res[0] = -res[0];
    res[3] = -res[3];

    return res == fig.getLimits();
}

template<typename FigureType>
class FigureTestSuite : public ::testing::Test {
public:
    using CurrentFigure = TestFigure<FigureType>;
};

using FigureTypes = ::testing::Types<SFigure,
        ZFigure,
        TFigure,
        LFigure,
        JFigure,
        LineFigure,
        SquareFigure>;

TYPED_TEST_SUITE(FigureTestSuite, FigureTypes);

TYPED_TEST(FigureTestSuite, FigureTypes) {
    typename TestFixture::CurrentFigure fig(Colors::kRed);

    for (size_t i = 0; i < 4; ++i) {
        fig.rotate();
        EXPECT_TRUE(checkLimits(fig));
    }
}

