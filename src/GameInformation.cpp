#include "GameInformation.hpp"

namespace strange_tetris {
    namespace _details {
        GameInformation::GameInformation(): time_for_iteration(std_time_for_iteration) {}

        GameInformation::milliseconds GameInformation::getTimeForIteration() const {
            return time_for_iteration;
        }

        void GameInformation::setTimeForIteration(GameInformation::milliseconds new_duration) {
            time_for_iteration = new_duration;
        }

        void GameInformation::changeTimeForIteration(GameInformation::milliseconds delta) {
            if (time_for_iteration + delta >= top_bound) {
                time_for_iteration = top_bound;
            } else if (time_for_iteration + delta <= bottom_bound) {
                time_for_iteration = bottom_bound;
            } else {
                time_for_iteration += delta;
            }
        }
    }
}