# StrangeTetris

A project which is made for the subject "Technologies of Programming"

## Preparation

Up to this moment, to build the game for console one need to have NCurses library packages: **libncurses5-dev** and **libncursesw5-dev**. Here is how to install them on some platforms:

- **Linux (Ubuntu, Debian)**: `sudo apt install libncurses5-dev libncursesw5-dev`

## Installation

- **Linux**: open a terminal and write these commands
```
git clone https://gitlab.com/DanMax03/strangetetris.git
cd strangetetris
mkdir build
cd build
cmake ..
```
If you also want to build tests, then use `cmake -DBUILD_TESTS=ON ..` command instead of the previous one

All created executables are contained in the **strangetetris/bin** folder. *StrangeTetris* is the game itself, *UTests* - google tests of the game

## Controls

- **a** and **d** to move a figure left or right respectively
- **s** to put down a figure
- **r** to rotate a figure

## UML diagrams

To see a diagram as a picture one should use the [plantuml website](https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000)

## Roadmap

- Add SFML GUI
- Make 2 new game modes: time attack and strange tetris
- Music

## Contributing
Let us be honest - you won't commit in this repo

## Authors and acknowledgment
Me, dark night and good music

## License
Coming soon

## Project status
Making changes from time to time, you know...
